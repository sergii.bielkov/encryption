﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AsymmetricEncription
{
    class Program
    {
        static void Main(string[] args)
        {
            var clientCertificate = GetX509CertificateFromStore("CN=ClientSide");
            var serverCertificate = GetX509CertificateFromStore("CN=ServerSide");

            // Encrypt data to send from client to server.
            var dataToSendFromClient = @"request for 1234 5678 9012 3456";
            var encryptedData = Encrypt(dataToSendFromClient, serverCertificate.PublicKey);
            // Sign data on client.
            var signatureFromClient = Sign(encryptedData, clientCertificate.PrivateKey);


            // sending to server .. vzghuuuuuh ..

            // Decrypt data on server.
            var dataReceivedOnServer = encryptedData;
            var signatureReceivedOnServer = signatureFromClient;

            var isAuthenticFromClient = VerifySignature(dataReceivedOnServer, signatureReceivedOnServer, clientCertificate.PublicKey);
            if (isAuthenticFromClient)
            {
                var decryptedDataOnServer = Decrypt(dataReceivedOnServer, serverCertificate.PrivateKey);

                // Encrypt server response.
                var dataToSendFromServer = @"sesponse for 1234 5678 9012 3456 request is good";
                var encryptedDataFromServer = Encrypt(dataToSendFromServer, clientCertificate.PublicKey);

                // sending back to client .. vzghuuuuuh ..

                // Decrypt data on server.
                var dataReceivedOnClient = encryptedDataFromServer;

                var decryptedDataOnClient = Decrypt(dataReceivedOnClient, clientCertificate.PrivateKey);
            }
        }

        private static X509Certificate2 GetX509CertificateFromStore(string certificateName, StoreName storeName = StoreName.My, StoreLocation storeLocation = StoreLocation.LocalMachine)
        {
            var store = new X509Store(storeName, storeLocation);

            store.Open(OpenFlags.ReadOnly);

            try
            {
                var certificate = store.Certificates.OfType<X509Certificate2>().FirstOrDefault(certificateItem => certificateItem.SubjectName.Name.Equals(certificateName, StringComparison.OrdinalIgnoreCase));
                if (certificate != null)
                {
                    return new X509Certificate2(certificate);
                }
                return null;
            }
            finally
            {
                store.Certificates.OfType<X509Certificate2>().ToList().ForEach(certificateItem => certificateItem.Reset());
                store.Close();
            }
        }

        public static string Encrypt(string data, PublicKey publicKey)
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);
            var provider = (RSACryptoServiceProvider)publicKey.Key;

            var encryptedData = provider.Encrypt(dataBytes, true);

            var encryptedDataStringified = Convert.ToBase64String(encryptedData);

            return encryptedDataStringified;
        }

        public static string Decrypt(string data, AsymmetricAlgorithm privateKey)
        {
            var dataBytes = Convert.FromBase64String(data);
            var provider = (RSACryptoServiceProvider)privateKey;

            var decryptedData = provider.Decrypt(dataBytes, true);

            var decryptedDataStringified = Encoding.UTF8.GetString(decryptedData);

            return decryptedDataStringified;
        }

        public static string Sign(string data, AsymmetricAlgorithm privateKey)
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);
            var provider = (RSACryptoServiceProvider)privateKey;

            var signatureBytes = provider.SignData(dataBytes, CryptoConfig.MapNameToOID("SHA1"));
            var signature = Convert.ToBase64String(signatureBytes);

            return signature;
        }

        public static bool VerifySignature(string data, string signature, PublicKey publicKey)
        {
            var dataBytes = Encoding.UTF8.GetBytes(data);
            var signatureBytes = Convert.FromBase64String(signature);
            var provider = (RSACryptoServiceProvider)publicKey.Key;

            var isAuthentic = provider.VerifyData(dataBytes, CryptoConfig.MapNameToOID("SHA1"), signatureBytes);

            return isAuthentic;
        }
    }
}
