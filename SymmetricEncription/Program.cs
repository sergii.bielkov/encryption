﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] symmetricKey = null;
            byte[] initializationVector = null;

            using (var cryptoProvider = new RijndaelManaged())
            {
                symmetricKey = cryptoProvider.Key;
                initializationVector = cryptoProvider.IV;
            }

            var dataToEncrypt = "Слава Україні!" +
                @"Прости мені, мій змучений народе, 
                що я мовчу.Дозволь мені мовчать!
                Бо ж сієш, сієш, а воно не сходе,
                І тільки змії кубляться й сичать.
                Всі проти всіх, усі ні з ким не згодні.
                Злість рухає людьми, 
                але у бік безодні.

                © Ліна Костенко";

            var message = EncryptData(dataToEncrypt, symmetricKey, initializationVector);
            var signature = SignData(message, symmetricKey);

            // sending data ..

            //using (var cryptoProvider = new RijndaelManaged())
            //{
            //    var dataToEncryptCorrupted = "Putin huilo! la la la la la la la la";

            //    message = EncryptData(dataToEncryptCorrupted, cryptoProvider.Key, cryptoProvider.IV);
            //    signature = SignData(message, cryptoProvider.Key);
            //}

            // sending data end ..

            var receivedEncryptedData = message;
            var receivedSignature = signature;

            var isAuthentic = VerifySignature(receivedSignature, receivedEncryptedData, symmetricKey);

            if (isAuthentic)
            {
                var decryptedData = DecryptData(receivedEncryptedData, symmetricKey, initializationVector);
            }
        }

        public static string SignData(string message, byte[] symmetricKey)
        {
            var messageBytes = Encoding.UTF8.GetBytes(message);
            using (HMACSHA256 hmac = new HMACSHA256(symmetricKey))
            {
                var signatureBytes = hmac.ComputeHash(messageBytes);
                string signature = Convert.ToBase64String(signatureBytes);
                return signature;
            }
        }

        public static bool VerifySignature(string signatureToVerify, string message, byte[] symmetricKey)
        {
            var messageBytes = Encoding.UTF8.GetBytes(message);
            using (HMACSHA256 hmac = new HMACSHA256(symmetricKey))
            {
                var signatureBytes = hmac.ComputeHash(messageBytes);
                string signature = Convert.ToBase64String(signatureBytes);

                if (signature.Equals(signatureToVerify, StringComparison.Ordinal))
                {
                    return true;
                }
                return false;
            }
        }

        public static string EncryptData(string dataToEncrypt, byte[] symmetricKey, byte[] initializationVector)
        {
            using (var cryptoProvider = new RijndaelManaged())
            {
                var dataToEncryptBytes = Encoding.UTF8.GetBytes(dataToEncrypt);
                var encryptor = cryptoProvider.CreateEncryptor(symmetricKey, initializationVector);
                var encryptedBytes = Transform(dataToEncryptBytes, encryptor);
                var encryptedDataToSend = Convert.ToBase64String(encryptedBytes);
                return encryptedDataToSend;
            }
        }

        public static string DecryptData(string dataToDecrypt, byte[] symmetricKey, byte[] initializationVector)
        {
            using (var cryptoProvider = new RijndaelManaged())
            {
                var dataToDecryptBytes = Convert.FromBase64String(dataToDecrypt);
                var decryptor = cryptoProvider.CreateDecryptor(symmetricKey, initializationVector);
                var decryptedBytes = Transform(dataToDecryptBytes, decryptor);
                var decryptedData = Encoding.UTF8.GetString(decryptedBytes);
                return decryptedData;
            }
        }

        private static byte[] Transform(byte[] bytesToTransform, ICryptoTransform cryptoTransform)
        {
            using(var buffer = new MemoryStream())
            {
                using(var cryptoStream = new CryptoStream(buffer, cryptoTransform, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(bytesToTransform, 0, bytesToTransform.Length);
                    cryptoStream.FlushFinalBlock();
                    return buffer.ToArray();
                }
            }
        } 
    }
}
